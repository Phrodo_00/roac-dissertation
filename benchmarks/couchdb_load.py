#!/usr/bin/env python
import time
from couchdb.client import Server
import json
from common import build_name
import common


DATABASE = 'test0'

server = Server()
db = server[DATABASE]

start = time.time()

for i in range(0, common.COUNT):
    name = build_name(common.DIRECTORY, i, 'json')
    with open(name) as f:
        data = f.read()
    data = json.loads(data)
    db.save(data)
end = time.time()
print(end - start)
