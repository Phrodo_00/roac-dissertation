#!/usr/bin/env python
import json
import os
from uuid import uuid1

from randomdocs import RandomDocument
from common import build_name
import common


if __name__ == '__main__':
    factory = RandomDocument()
    os.mkdir(common.DIRECTORY)
    for i in range(0, common.COUNT):
        document = factory.gen_dict()
        document_var = factory.dict_variation(document)

        name = build_name(common.DIRECTORY, i, 'json')
        name_var = build_name(common.DIRECTORY, i, 'json', prefix='var')

        _id = uuid1().hex

        for d, n in ((document, name), (document_var, name_var)):
            d['_id'] = _id
            with open(n, 'w') as f:
                json.dump(d, f, indent=4, separators=(',', ': '))
