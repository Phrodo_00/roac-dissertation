#!/usr/bin/env python

import random
import json


class RandomWords(object):
    def __init__(self, filename='/usr/share/dict/words', lazy_load=True):
        self.filename = filename
        if not lazy_load:
            self.words

    @property
    def words(self):
        if not hasattr(self, '_words'):
            with open(self.filename) as wordfile:
                self._words = [word.strip() for word in wordfile]
        return self._words

    def get_words(self, n):
        return random.sample(self.words, n)

    def get_word(self):
        return random.choice(self.words)

    def __call__(self):
        return self.get_word()

    def __iter__(self):
        return self

    def __next__(self):
        return self.get_word()


class RandomDocument(object):

    simpletypes = ['int', 'string', 'float', 'bool', 'none']
    compositetypes = ['list', 'dict']

    def __init__(self, words=None, dict_nodes=(2, 10),
                 list_nodes=(2, 10), levels=4):
        if words:
            self.words = words
        else:
            self.words = RandomWords()
        self.dict_nodes = dict_nodes
        self.list_nodes = list_nodes
        self.levels = levels

    def gen_dict(self, levels=None):
        if levels is None:
            levels = self.levels
        # Select size of dict
        size = random.randint(*self.dict_nodes)
        d = {}
        for i in range(size):
            d[self.words()] = self.gen_node(levels - 1)
        return d

    def gen_list(self, levels=None):
        if levels is None:
            levels = self.levels
        # Select size of dict
        size = random.randint(*self.list_nodes)
        a = []
        for i in range(size):
            a.append(self.gen_node(levels - 1))
        return a

    def gen_node(self, levels=0):
        # Choose type of node
        if levels < 1:
            kind_of_node = random.choice(self.simpletypes)
        else:
            kind_of_node = random.choice(
                self.simpletypes + self.compositetypes)

        # Generate node
        if kind_of_node == 'float':
            return random.random()
        elif kind_of_node == 'int':
            return random.randrange(-65535, 65535)
        elif kind_of_node == 'string':
            return self.words()
        elif kind_of_node == 'bool':
            if random.random() > 0.5:
                return True
            else:
                return False
        elif kind_of_node == 'none':
            return None
        elif kind_of_node == 'list':
            return self.gen_list(levels)
        elif kind_of_node == 'dict':
            return self.gen_dict(levels)

    def dict_variation(self, dict_, levels=None):
        if levels is None:
            levels = self.levels - 1

        n = random.randrange(len(dict_)) + 1
        new_keys = random.sample(dict_, n)
        new_dict = {}
        for key in new_keys:
            new_dict[key] = self.gen_node(levels=levels)
        return new_dict


if __name__ == '__main__':
    factory = RandomDocument()
    document = factory.gen_dict()
    print(json.dumps(document, indent=4, separators=(',', ': ')))
