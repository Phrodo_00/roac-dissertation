#!/usr/bin/env python
import time
import psycopg2
from psycopg2.extras import Json
import json
from common import build_name
import common


CONNSTRING = 'dbname=json_test user=dev'


start = time.time()

conn = psycopg2.connect(CONNSTRING)
cur = conn.cursor()

for i in range(0, common.COUNT):
    name = build_name(common.DIRECTORY, i, 'json')
    with open(name) as f:
        data = f.read()
    data = json.loads(data)
    cur.execute('INSERT INTO data (json) VALUES (%s)',
                (Json(data),))
conn.commit()
cur.close()
conn.close()
end = time.time()
print(end - start)
