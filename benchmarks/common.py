import os

DIRECTORY = 'docs'
COUNT = 10000
EXTENSION = 'json'
VAR_PREFIX = 'var'


def build_name(directory, i, extension, prefix='',):
    return os.path.join(directory, '%s%05d.%s' % (prefix, i, extension))


def ori_name(i):
    return build_name(DIRECTORY, i, EXTENSION)


def var_name(i):
    return build_name(DIRECTORY, i, EXTENSION, prefix=VAR_PREFIX)
