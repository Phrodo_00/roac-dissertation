#!/usr/bin/env python
import time
import json
from pymongo import MongoClient
from common import build_name
import common


DATABASE = 'test0'


client = MongoClient()
db = client.test0
collection = db.tests

start = time.time()

for i in range(0, common.COUNT):
    name = build_name(common.DIRECTORY, i, 'json')
    with open(name) as f:
        data = f.read()
    data = json.loads(data)
    collection.insert(data)
end = time.time()
print(end - start)
