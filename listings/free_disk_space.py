import os

@app.script_handler_by_name('^space.pl$')
def free_disk_space(result):
    if result.data['/']['use%'] > 90:
        # Borrar los contenidos de la carpeta /tmp/
        for root, dirs, files in os.walk('/tmp/', topdown=False):
            for name in files:
                os.remove(os.path.join(root, name))
            for name in dirs:
                os.rmdir(os.path.join(root, name))
