library(plyr)
library(ggplot2)

results <- read.csv('bench_results.csv')
summ <- ddply(results, c("Motor"), summarise, N=length(Tiempo),
              mean=mean(Tiempo), sd=sd(Tiempo), se=sd/sqrt(N))

postscript("../bench.eps", horizontal=FALSE, onefile=FALSE, paper="special",
           width=5.5, height=4)

ggplot(summ, aes(x=Motor, y=mean)) +
    geom_bar(position=position_dodge(), stat="identity", fill="#C02942",
             size=.3) +
    geom_errorbar(aes(ymin=mean-se, ymax=mean+se),
                  position=position_dodge(.9), color="black", size=.3,
                  width=.2) +
    xlab("Motor de base de datos") +
    ylab("Tiempo [s]") +
    ggtitle("Tiempo en almacenar 10000 documentos\nde forma secuencial") +
    theme_bw()

dev.off()

results <- read.csv('bench_results2.csv')
summ <- ddply(results, c("Motor"), summarise, N=length(Tiempo),
              mean=mean(Tiempo), sd=sd(Tiempo), se=sd/sqrt(N))

postscript("../bench2.eps", horizontal=FALSE, onefile=FALSE, paper="special",
           width=5.5, height=4)

ggplot(summ, aes(x=Motor, y=mean)) +
    geom_bar(position=position_dodge(), stat="identity", fill="#C02942",
             size=.3) +
    geom_errorbar(aes(ymin=mean-se, ymax=mean+se),
                  position=position_dodge(.9), color="black", size=.3,
                  width=.2) +
    xlab("Motor de base de datos") +
    ylab("Tiempo [s]") +
    ggtitle("Tiempo en almacenar 10000 documentos\nde forma secuencial") +
    theme_bw()

dev.off()
